# Description

This program creates OpenVPN `.ovpn` files that can be used to connect to your OpenVPN server.
The `.ovpn` files are created using TLS/SSL certificates & keys which are generated and signed using OpenVPN.


# Dependencies

The following packages are required to generate the `.crt` and `.key` files using OpenVPN:

* openvpn
* easy-rsa

#### Debian Linux

`$ sudo apt-get install openvpn easy-rsa`

#### Everything Else

[OpenVPN Downloads](https://openvpn.net/index.php/open-source/downloads.html)


# Set Up
The official OpenVPN documentation can be found [here](https://openvpn.net/index.php/open-source/documentation/howto.html), but I've provided some basic instructions that should make it easier to get going.

If you've already generated your keys & certificates, you can skip to [Prepare for Launch](#Prepare-For-Launch).

## Create a CA Directory

This will create a Certificate Authority directory called `ca` that contains everything needed to generate key-pairs:
```
make-cadir ca
cd ca
```

## Vars File

If you want to change the **publicly visible** values that are included in each certificate, you can edit the following lines in the `vars` file. The values can be anything you like, just don't leave them blank.

```
export KEY_COUNTRY="US"
export KEY_PROVINCE="CA"
export KEY_CITY="SanFrancisco"
export KEY_ORG="Company"
export KEY_EMAIL="name@host.domain"
export KEY_OU="Department"

export KEY_NAME="server"
```

Next, set the `vars` file as the source file:

`$ source vars`

> **Note**:  
> If you get the error `No openssl.cnf file could be found. Further invocations will fail`, you can fix it by creating a symlink to the `openssl-1.0.0.cnf` file:
> 
> `$ ln openssl-1.0.0.cnf openssl.cnf `

## Generate CA File

The Certificate Authority file is the certificate that belongs to the machine that you're using to generate these files. It identifies the machine as a trusted issuer of keys & certificates. It's important to note that the integrity of the VPN connection will only be as secure as this machine.

`./build-ca`

This will prompt you to verify or change the values in the `vars` file. You can just press enter for each one assuming you configured the `vars` file.

## Generate Server Key

This will generate the server key & certificate files `server.key` and `server.crt`:

`./build-key-server server`

You can use another name besides `server` if you'd like, just know that it will be used as the name of your server.

## Generate Diffie–Hellman Parameters

This is the [secret color](https://i.imgur.com/1Gi8bmy.png) that gets mixed with both server and client keys in order to verify the connection.

`$ ./build-dh`

This will probably take a few minutes. Once it's complete, it will generate a file named similarly to `dh2048.pem`. I'll refer to this file as `dh*.pem`.

## Generate Secret TLS Auth Key

Also known as a static key

`$ openvpn --genkey --secret ta.key`

# Configure Server

Now you will need to configure your OpenVPN server using the generated files. The process will be different depending on what kind of server you're using (router interface, Linux, Windows, etc.), so you'll need to find more specific instructions to fit your setup. 

General instructions are to copy the `ca.crt`, `server.crt`,`server.key`, `dh*.pem` and `ta.key` files, or their contents, onto your server along with a configuration file and start an OpenVPN service. An example server configuration file can be found [here](https://github.com/OpenVPN/openvpn/blob/master/sample/sample-config-files/server.conf).

You will also likely need to set up a Dynamic-DNS (DDNS) service that will always point to your public IP since most internet providers change IP's every so often.

# Generate Client Files

This will generate `client.key` & `client.crt` files:

`$ ./build-key client`

You can use another name besides `client`, just know it will be used to identify this specific key-pair.

You can create as many pairs as you like, just be sure to use a different name for each one.

## Configure Client

The `settings.cfg` file included with this project is the client configuration file, and should compliment the server configuration. Your server may be using a server configuration file, a GUI, or a web interface.

An example client configuration file can be found [here](https://github.com/OpenVPN/openvpn/blob/master/sample/sample-config-files/client.conf).

Here is what my configuration file ended up looking like:

```
client
dev tun
proto tcp
remote me.ddns-host.com 1194
cipher AES-128-CBC
tls-cipher TLS-DHE-RSA-WITH-AES-256-CBC-SHA
comp-lzo
auth-nocache
remote-cert-tls server
float
```

You'll likely need to do some tweaking to to get the ideal setup. OpenVPN will let you know if there are any errors and you can then adjust your configuration if needed.


# Prepare for Launch

Make sure you have correctly done the following:

 - [ ] Generated Server key-pairs
 - [ ] Configured Server
 - [ ] Generated Client key-pairs
 - [ ] Configured `settings.cfg` Client file
 - [ ]  Placed `create_ovpn_profiles.py` in the same directory as the Client key-pairs

# Usage

Just execute the `create_ovpn_profiles.py` file:

`$ python create_ovpn_profiles.py`

A single `.ovpn` file should be created for each key/certificate pair in the same directory. These files can be used with an OpenVPN app or service to set up a secure VPN connection to your server from virtually any device.

> **Warning!**  
Assuming you've configured everything properly, each `.ovpn` file is all that is required to connect to your network from **anywhere in the world**. Huge care should be used to keep these files secure. 

