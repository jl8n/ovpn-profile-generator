#!/usr/bin/env python3
#
# Please read the included README.md
# Author: Josh Layton @jl8n

import os


def get_config(filename):
    """Return a list of configuration settings read from a file"""
    try:
        with open(filename, 'r') as f:
            lines = f.readlines()
    except FileNotFoundError as e:
        print(e)
        exit(1)
    else:
        configs = []

        for line in lines:
            configs.append(line.rstrip())

        return configs


def get_key_cert_pairs():
    """Return a list of all key/certificate pairs found in local dir"""
    files = os.listdir('.')
    key_cert_pairs = []

    # Identifies pairs by filename
    for i in range(len(files) - 1):
        if files[i].endswith('.crt'):
            filename_i = os.path.splitext(files[i])[0]

            for j in range(i + 1, len(files) - 1):
                filename_j = os.path.splitext(files[j])[0]

                if filename_i == filename_j and files[j].endswith('.key'):
                    key_cert_pairs.append((files[i], files[j]))

    return key_cert_pairs


def get_openvpn_files(ca_cert="ca.crt", secret_key="ta.key"):
    """
    Return a list of dictionaries, each containing the required files
    necessary for generating individual OpenVPN profiles
    """

    if not os.path.isfile(ca_cert):
        print("{} not found".format(ca_cert))
        exit(1)
    if not os.path.isfile(secret_key):
        print("{} not found".format(secret_key))
        exit(1)

    all_openvpn_profiles = []

    for key, cert in get_key_cert_pairs():
        openvpn_profile_files = {
            "key": key,
            "cert": cert,
            "ca": ca_cert,
            "secret": secret_key
        }
        all_openvpn_profiles.append(openvpn_profile_files)

    return all_openvpn_profiles


def get_cert_from_file(filename):
    """Isolate and return a single PEM certificate from a file."""
    try:
        with open(filename, 'r') as f:
            lines = f.readlines()
    except FileNotFoundError as e:
        print(e)
        exit(1)
    else:
        cert = ""
        start_index = None
        end_index = None

        for i in range(len(lines) - 1):
            if "-----BEGIN" == lines[i][:10]:
                start_index = i
            if "-----END" == lines[i][:8]:
                end_index = i

        for line in lines[start_index:end_index]:
            cert += line

        return cert


def main():
    config_file = "settings.cfg"
    configs = get_config(config_file)
    openvpn_files = get_openvpn_files()

    for profile in openvpn_files:
        tags = ""

        for key in profile:
            certificate = get_cert_from_file(profile[key])

            if key == "key":
                tag = "<key>\n{}</key>\n".format(certificate)
            elif key == "cert":
                tag = "<cert>\n{}</cert>\n".format(certificate)
            elif key == "ca":
                tag = "<ca>\n{}</ca>\n".format(certificate)
            elif key == "secret":
                tag = "<tls-auth>\n{}</tls-auth>\n".format(certificate)

            tags += tag

        profile_name = os.path.splitext(profile["key"])[0]

        with open("{}.ovpn".format(profile_name), "w") as f:
            for setting in configs:
                f.write(setting + "\n")

            f.write("\n")
            f.write(tags)
            print("Created {}.ovpn".format(profile_name))


if __name__ == "__main__":
    main()
